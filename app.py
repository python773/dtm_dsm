import sys, os
from PyQt5.QtWidgets import QApplication, QMainWindow,QFileDialog
from modules.digital_process import DigitalProcess
from ui.window import Ui_MainWindow
from modules.postgres import database_list, connection_test
from modules.functions import permission_checker, check_shp_in_folder, listing, files_parameters
from modules.mylogger import MyLogger
from PyQt5.QtCore import QRunnable, Qt, QThreadPool

class Window(QMainWindow, Ui_MainWindow):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setupUi(self)
        self._connect_signals_slots()

    def _setup_UI(self):
        self.setupUi(self)
        self._update_initial_state()

    def _connect_signals_slots(self):
        self.read_path_button.clicked.connect(self._load_files)                     #read files button
        self.ndsm.toggled.connect(self._update_ndsm_creation)                        #ndsm auto check dsm, dtm
        self.action_clean_form.triggered.connect(self._update_initial_state)        #auto clean 
        self.action_Exit.triggered.connect(self._exit)                              #exit
        self.clip_option.currentTextChanged.connect(self._database_enable_options)  #clip option enable
        self.host_input.textActivated.connect(self._get_database_list)         #get db list if port or host name changed
        self.port_input.textActivated.connect(self._get_database_list)         #get db list if port or host name changed
        self.run_button.clicked.connect(self._run)
    
    def _run(self):
        if not self._check_settings_before_run(): #check settings
            settings = self._get_settings()
            files = listing(settings['input_path'], 'las', True)
            files_params = files_parameters(files, settings)
            threadCount = QThreadPool.globalInstance().maxThreadCount()

            pool = QThreadPool.globalInstance()
            for i in files_params:
                # 2. Instantiate the subclass of QRunnable
                runnable = DigitalProcess(i)
                # 3. Call start()
                pool.start(runnable)
            #for i in files_params:
                #DigitalProcess(i)

    def _update_initial_state(self):
        self.dtm.setChecked(False)
        self.dsm.setChecked(False)
        self.ndsm.setChecked(False)
        self.cell_size_input.setText('0.5')
        self.ground_classes_input.setText('2,12')
        self.coordinate_system_input.setText('32366')
        self.close_gaps_group.setChecked(True)
        self.close_gaps_thr_input.setText('0.100000')
        self.gaussian_group.setChecked(True)
        self.sigma_input.setText('1')
        self.kernel_type_input.setText('1')
        self.kernel_radius_input.setText('5')
        self.db_connection_group.setEnabled(False)
        self.clip_option.setCurrentText("without_clip")
        self.database_input.clear()
        self.host_input.setCurrentText('')
        self.port_input.setCurrentText('')
        self.asc_output.setChecked(False)
        self.xyz_output.setChecked(False)
        self.tif_output.setChecked(False)
        self.tif_hillshade_output.setChecked(False) 

    def _database_enable_options(self,status):
        if status=='clip_to_database_buffer':
            self.db_connection_group.setEnabled(True)
            self.buffer_r_input.setEnabled(True)
        else:
            self.db_connection_group.setEnabled(False)
            self.buffer_r_input.setEnabled(False)
    
    def _update_ndsm_creation(self):
        if self.ndsm.isChecked():
            self.dtm.setChecked(True)
            self.dsm.setChecked(True)
        else:
            self.dtm.setChecked(False)
            self.dsm.setChecked(False)

    def _status_message(self, message):
        if 'ERROR' in message.upper():
            self.statusBar.setStyleSheet("color : red")
        else:
            self.statusBar.setStyleSheet("color : green")
        self.statusBar.showMessage("\t"*2+message)

    def _get_database_list(self):
        if self.host_input.currentText() and len(self.port_input.currentText())==4:
            dbs, status=database_list(self.host_input.currentText(),
                                      self.port_input.currentText())
            self._status_message(status)
            self.database_input.addItems(dbs)

    def _exit(self):
        app.quit()

    def _load_files(self):
        if self.path_input.text():
            initDir = self.path_input.text()
        else:
            initDir = str(os.getcwd())
        folder = QFileDialog.getExistingDirectory(self, "Choose Folder Path", initDir)
        if folder: self.path_input.setText(folder)

    def _get_settings(self):
        settings=dict()
        settings['input_path'] = self.path_input.text()
        settings['dsm_creation'] = self.dsm.isChecked()
        settings['dtm_creation'] = self.dtm.isChecked()
        settings['ndsm_creation'] = self.ndsm.isChecked()
        settings['cell_size'] = self.cell_size_input.text()
        settings['ground_classes'] = self.ground_classes_input.text()
        settings['epsg'] = self.coordinate_system_input.text()
        settings['close_gaps'] = self.close_gaps_group.isChecked()
        settings['close_gaps_threshold'] = self.close_gaps_thr_input.text()
        settings['gaussian_filter'] = self.gaussian_group.isChecked()
        settings['sigma'] = self.sigma_input.text()
        settings['kernel_type'] = self.kernel_type_input.text()
        settings['kernel_radius'] = self.kernel_radius_input.text()
        settings['clip_option'] = self.clip_option.currentText()
        settings['buffer_r'] = self.buffer_r_input.text()
        settings['host'] = self.host_input.currentText()
        settings['port'] = self.port_input.currentText()
        settings['database'] = self.database_input.currentText()
        settings['asc'] = self.asc_output.isChecked()
        settings['xyz'] = self.xyz_output.isChecked()
        settings['tif'] = self.tif_output.isChecked()
        settings['tif_hillshade'] = self.tif_hillshade_output.isChecked()
        return settings
    
    def _check_settings_before_run(self):
        status = ''
        self._status_message('')                                            #clean status bar
        
        if len(self.path_input.text())==0:                                  #check existing input path
            status = 'Error - please read input path'
        else:
            permission = permission_checker(self.path_input.text())             #check accessable of input path
            if not len(permission)==0: status = permission


        if (self.dsm.isChecked()==False and self.dtm.isChecked()==False     #check select at least one creation module
                                     and self.ndsm.isChecked()==False):
            status = 'Error - please choose at least one Creation module'


        if len(self.cell_size_input.text())==0:                             #check cell size value
            status = 'Error - please fill cell size value'
        else:
            try:
                float(self.cell_size_input.text())
            except ValueError as e:
                status = 'Error - please put correct number value in cell size input box'


        if len(self.close_gaps_thr_input.text())==0:                        #check close gaps threshold
            status = 'Error - please fill close gaps threshold value'
        else:
            try:
                float(self.close_gaps_thr_input.text())
            except ValueError as e:
                status = 'Error - please put correct number value in close gaps threshold input box'   


        if len(self.ground_classes_input.text())==0 and self.dtm.isChecked():#check ground classes
            status = 'Error - please fill ground classes'
        
        elif self.dtm.isChecked() and (" " in self.ground_classes_input.text() or "." in self.ground_classes_input.text()):
            status = 'Error - please insert ground classes with comma separator'


        if len(self.coordinate_system_input.text())==0:                        #check EPSG
            status = 'Error - please fill Coordinate System EPSG value'
        else:
            try:
                float(self.coordinate_system_input.text())
            except ValueError as e:
                status = 'Error - please put correct number value in Coordinate System EPSG input box' 
        

        if self.gaussian_group.isChecked() and len(self.sigma_input.text())==0: #check sigma
            status = 'Error - please fill Sigma value'
        elif self.gaussian_group.isChecked():
            try:
                float(self.sigma_input.text())
            except ValueError as e:
                status = 'Error - please put correct number value in Sigma input box'
        
        
        if self.gaussian_group.isChecked() and len(self.kernel_type_input.text())==0: #check Kernel Type
            status = 'Error - please fill Kernel Type value'
        elif self.gaussian_group.isChecked():
            try:
                float(self.kernel_type_input.text())
            except ValueError as e:
                status = 'Error - please put correct number value in Kernel Type input box'


        if self.gaussian_group.isChecked() and len(self.kernel_radius_input.text())==0: #check  Kernel Radius
            status = 'Error - please fill Kernel Radius value'
        elif self.gaussian_group.isChecked():
            try:
                float(self.kernel_radius_input.text())
            except ValueError as e:
                status = 'Error - please put correct number value in Kernel Radius input box'
        

        if self.clip_option.currentText()=='clip_to_database_buffer' and len(self.host_input.currentText())==0:  #Check host
            status = 'Error - please fill host name'

        elif self.clip_option.currentText()=='clip_to_database_buffer' and len(self.port_input.currentText())==0:  #Check port
            status = 'Error - please fill port number'
        
        elif self.clip_option.currentText()=='clip_to_database_buffer' and len(self.database_input.currentText())==0:  #Check port
            status = 'Error - please select database name'

        else:
            if not connection_test(self.host_input.currentText(),self.port_input.currentText(),self.database_input.currentText()):
                status= f"Error - can't connect to database {self.database_input.currentText()}"


        if self.clip_option.currentText()=='clip_to_database_buffer' and len(self.buffer_r_input.text())==0:  #Check buffer
            status = 'Error - please fill Buffer R value'
        elif self.clip_option.currentText()=='clip_to_database_buffer':
            try:
                float(self.buffer_r_input.text())
            except ValueError as e:
                status = 'Error - please put correct number value in Buffer R input box'
        

        if self.clip_option.currentText()=='clip_to_shp':                                                   #Check clip to SHP
            status = check_shp_in_folder(self.path_input.text())


        if (not self.asc_output.isChecked() and not self.xyz_output.isChecked()                             #Check output
            and not self.tif_output.isChecked() and not self.tif_hillshade_output.isChecked()):
            status = 'Error - please select at least one output format'
        
        #send message to log and GUI
        if len(status) > 0:
            logger.error(status)
            self._status_message(status)
        return status

if __name__ == "__main__":
    logger=MyLogger()
    app = QApplication(sys.argv)
    win = Window()
    win.show()
    sys.exit(app.exec())