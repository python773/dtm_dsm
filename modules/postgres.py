import psycopg2
import pandas as pd
from shapely.geometry import Point, LineString
import geopandas as gpd
from modules.functions import prj_creator
import os

def query_execute(_host, _port, _database, _query):
    try:
        status=''
        output=[]
        connection = psycopg2.connect(user="postgres",
                                    password="postgres",
                                    host=_host,
                                    port=_port,
                                    database=_database)
        connection.set_client_encoding('ISO-8859-1')
        cursor = connection.cursor()
        cursor.execute(_query)
        output = cursor.fetchall()
        status=f"Correct connect to db: {_database} and executed query {_query}"
        cursor.close()
        connection.close()
        return output, status

    except (Exception, psycopg2.Error) as error:
        status=f"Error while fetching data from {_database} due to error: {error}"
        return output, status

def database_list(_host, _port):
    result, status=query_execute(_host, _port, '', 'SELECT datname FROM pg_database;')
    if status.startswith("Correct"):
        result = [_[0] for _ in result] #list of tuple to list
        status='Correct imported database list'
    return result, status

def get_powerline_buffer(host, port, database, powerline, buffer, output_path, EPSG):
    query = f'''
            SELECT 
                pow.name AS powerline,
                (SELECT poles.ground_point FROM poles WHERE id=pic1.id_pole) AS p1_xy,
                (SELECT poles.ground_point FROM poles WHERE id=pic2.id_pole) AS p2_xy

            FROM powerlines pow,
                 catenaries c,
                 polesincable pic1,
                 polesincable pic2,
                 cables cab,
                 voltage v
            WHERE 
                c.id_pole1 = pic1.id_polincab
                AND c.id_pole2 = pic2.id_polincab
                AND pic1.id_cable = pic2.id_cable
                AND cab.id_powerline = pow.id
                AND pic1.id_cable = cab.id
                AND pow.id_voltage=v.id
                AND pow.name like '{powerline}'
            GROUP BY 
                pic1.id_pole,
                pic2.id_pole,
                pow.name,
                cab.type,
                v.voltage,
                pow.description
            '''
    powerline_db, status = query_execute(host, port, database, query)

    #dataframe from powerline_db list
    df=pd.DataFrame(powerline_db, columns=['powerline', 'p1_xy','p2_xy'])
    
    #remove duplicates
    df['span'] = df['p1_xy'] + df['p2_xy']
    df['span2'] = df['p2_xy'] + df['p1_xy']
    df=df.drop_duplicates(subset=['span','span2'])

    #points extract
    df['p1_xy'] = df['p1_xy'].str.strip('()')
    df['p2_xy'] = df['p2_xy'].str.strip('()') 
    df[['p1_x','p1_y']] = df['p1_xy'].str.split(',',expand=True).astype('float64')
    df[['p2_x','p2_y']] = df['p2_xy'].str.split(',',expand=True).astype('float64')
    geometry_p1 = [Point(xy) for xy in zip(df.p1_y, df.p1_x)]
    geometry_p2 = [Point(xy) for xy in zip(df.p2_y, df.p2_x)]
    geometry=[LineString(xy) for xy in zip(geometry_p1, geometry_p2)]

    #create geopandas
    gdf = gpd.GeoDataFrame(df, geometry=geometry)
    #create buffor
    gdf['geometry'] = gdf.buffer(buffer)
    #dissolve
    gdf = gdf.dissolve('powerline')
    #Export to shapefile
    output = os.path.join(output_path,powerline+'.shp')
    gdf.to_file(output)
    prj_creator(output, EPSG)
    return status, output

def connection_test(_host, _port, _database):
    try:
        conn = psycopg2.connect(user="postgres",
                                    password="postgres",
                                    host=_host,
                                    port=_port,
                                    database=_database,
                                    connect_timeout=1)
        conn.close()
        return True
    except:
        return False

#test=get_powerline_buffer('megadeth', '5433','771-swe_eon',"14308__40KV_SÖRBY-BODA-HJORTKVARN", 10,r'd:\MSZ\_Repos\DTM_DSM_project\test_batch\14308__40KV_SÖRBY-BODA-HJORTKVARN\_temp')
#test2=connection_test('firenze', '5432','806-it_terna_friuli_venezia_giulia')

