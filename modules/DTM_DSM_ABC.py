import subprocess,os, shutil, re
from osgeo_utils.gdal_calc import *
from modules.functions import createTemp


from PyQt5.QtCore import QRunnable

class DtmDsmProcess(QRunnable):

    SAGA_PATH=r"modules\saga-6.0.0_x64"
    LAS_TOOLS_PATH=r"modules\LAStools\bin"
    GDAL=r"venv\Lib\site-packages\osgeo"

    @staticmethod
    def execute_command(command):
        proc = subprocess.Popen(command, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
        out, err = proc.communicate()
        return str(out), str(err)


    @staticmethod        
    def las_version(las_file):
        status=''
        #new name and folder place
        las_file_name=os.path.basename(las_file)
        temp=createTemp(las_file)
        output_file=os.path.join(temp,las_file_name.split(".")[0]+"_1_2.las")

        #check version from lasinfo
        check_version_cmd = f'{DtmDsmProcess.LAS_TOOLS_PATH}\lasinfo.exe -i {las_file} -no_check -stdout'
        check_version, error_info = DtmDsmProcess.execute_command(check_version_cmd)
        version=[_.split()[2] for _ in check_version.split("\\r\\n") if "version major.minor:" in _][0]

        #convert version to 1.2 with las2las
        if version !='1.2':
            try:
                convert_cmd = f'{DtmDsmProcess.LAS_TOOLS_PATH}\las2las -i {las_file} -set_version 1.2 -o {output_file}'
                convert, error_convert = DtmDsmProcess.execute_command(convert_cmd)
                status=f"Converted file: {las_file_name} from LAS version {version} to version 1.2 and move to temp folder"
            except Exception as e:
                status=f"Can't convert file: {las_file_name} from LAS version {version} to version 1.2 and move to temp folder  due to error:\n {e}"
        else:
            try:
                shutil.copyfile(las_file, output_file)
                status=f"Copied file: {las_file_name} to temp folder"
            except Exception as e:
                status=f"Can't copy to temp folder file: {las_file_name} due to error:\n {e}"
        return status, output_file


    @staticmethod 
    def read_las(las_file):
        status=''
        output_file=las_file.replace(".las",".sg-pts")
        las_file_name=os.path.basename(las_file)
        
        try:
            read_las_cmd=f'{DtmDsmProcess.SAGA_PATH}\saga_cmd.exe --flags=r io_shapes_las "Import LAS Files" -FILES:"{las_file}" -POINTS:"{output_file}" -n:1 -r:1 -c:1 -VALID:0'
            read_las, error_read = DtmDsmProcess.execute_command(read_las_cmd)
            status=f"Read LAS file: {las_file_name}"
        except Exception as e:
            status=f"Can't read LAS file: {las_file_name} due to error:\n {e}"
        return status, output_file


    @staticmethod 
    def filter_first_return(pts_file, suffix=''):
        status=''
        suffix = "_"+suffix if len(suffix)>0 else suffix
        output_file=pts_file.replace("_1_2.sg-pts",suffix + "__first_return.sg-pts")
        pts_file_name=os.path.basename(pts_file)
        
        try:
            first_return_cmd=f'{DtmDsmProcess.SAGA_PATH}\saga_cmd --flags=r pointcloud_tools "Point Cloud Reclassifier / Subset Extractor" \
                                -INPUT:"{pts_file}" -ATTRIB:"number of the return" -MODE:1 -METHOD:0 -OLD:1 -SOPERATOR:0 -RESULT:"{output_file}"'
            first_return, error_first_return = DtmDsmProcess.execute_command(first_return_cmd)
            status=f"Filtered first return from file: {pts_file_name}"
        except Exception as e:
            status=f"Can't filter first return from file: {pts_file_name} due to error:\n {e}"
        return status, output_file


    @staticmethod 
    def filter_ground_classes(pts_file, ground_classes, suffix=''):
        status=''
        suffix = "_"+suffix if len(suffix)>0 else suffix
        output_file=pts_file.replace("_1_2.sg-pts",suffix + "__ground.sg-pts")
        pts_file_name=os.path.basename(pts_file)
        if len(ground_classes)>0:
            #prepare table with ground classes codes
            ground_classes=ground_classes.split(",")
            table=os.path.dirname(output_file)+"\\"+pts_file_name.split(".")[0]+"__table.txt"
            with open(table,'w') as t:
                t.write("minimum\tmaximum\tvalue")
                for _ in ground_classes:
                    t.write(f"\n{_}.000000\t{_}.000000\t{_}.000000")

            try:
                ground_cmd=f'{DtmDsmProcess.SAGA_PATH}\saga_cmd pointcloud_tools 6 -INPUT={pts_file} -ATTRIB="classification" -RESULT={output_file} -MODE=1 -METHOD=3 -TOPERATOR=1 -RETAB_2="{table}" -F_MIN=0 -F_MAX=1 -F_CODE=2'
                ground, error_ground = DtmDsmProcess.execute_command(ground_cmd)
                status=f"Filtered ground classes from file: {pts_file_name}"
            except Exception as e:
                status=f"Can't filter ground classesfrom file: {pts_file_name} due to error:\n {e}"
            
        else:
            os.rename(pts_file,output_file)
        return status, output_file

    @staticmethod 
    def grid_creation(pts_file, DTM_DSM, cell_size: float):
        status=''
        output_file=re.sub('__(?!.*__).*', f"__grid_{cell_size}.sgrd", pts_file)
        pts_file_name=os.path.basename(pts_file)

        #assign aggreation = 3 - lowest z if DTM and 4 highest Z for DSM
        aggregation = 3 if DTM_DSM=='DTM' else 4
    
        try:
            grid_creation_cmd=f'{DtmDsmProcess.SAGA_PATH}\saga_cmd --flags=r pointcloud_tools "Point Cloud to Grid" -POINTS:"{pts_file}" \
                                                            -GRID:"{output_file}" -OUTPUT:0 -AGGREGATION:{aggregation} -CELLSIZE:{cell_size}'
            grid_creation, error_grid_creation = DtmDsmProcess.execute_command(grid_creation_cmd)
            status=f"Grid creation from file: {pts_file_name}"
        except Exception as e:
            status=f"Can't create grid from file: {pts_file_name} due to error:\n {e}"
        return status, output_file


    @staticmethod 
    def close_gaps(grid_file, threshold=0.100000):
        status=''
        output_file=re.sub('__(?!.*__).*', f"__closed_gaps.sgrd", grid_file)
        grid_file_name=os.path.basename(grid_file)

        try:
            close_gaps_cmd=f'{DtmDsmProcess.SAGA_PATH}\saga_cmd --flags=r grid_tools "Close Gaps" -INPUT:"{grid_file}" -RESULT:"{output_file}" -THRESHOLD:{threshold}'
            close_gaps, error_close_gaps = DtmDsmProcess.execute_command(close_gaps_cmd)
            status=f"Closed gaps in grid file: {grid_file_name}"
        except Exception as e:
            status=f"Can't close gaps in file: {grid_file_name} due to error:\n {e}"
        return status, output_file


    @staticmethod 
    def gaussian_filter(grid_file, sigma=1, kernel_type=1, kernel_radius=5):
        status=''
        output_file=re.sub('__(?!.*__).*', f"__gaussian.sgrd", grid_file)
        grid_file_name=os.path.basename(grid_file)

        try:
            gaussian_cmd=f'{DtmDsmProcess.SAGA_PATH}\saga_cmd --flags=r grid_filter "Gaussian Filter" -INPUT:"{grid_file}" -RESULT:"{output_file}"\
                                                                        -SIGMA:{sigma} -KERNEL_TYPE:{kernel_type} -KERNEL_RADIUS:{kernel_radius}'
            gaussian, error_gaussian = DtmDsmProcess.execute_command(gaussian_cmd)
            status=f"Gaussian filter executed on grid file: {grid_file_name}"
        except Exception as e:
            status=f"Can't execute gaussian filter on grid file: {grid_file_name} due to error:\n {e}"
        return status, output_file
    
    
    @staticmethod 
    def clip_saga(grid_file, shp_file):
        status=''
        output_file=re.sub('__(?!.*__).*', f"__clipped.sgrd", grid_file)
        grid_file_name=os.path.basename(grid_file)

        try:
            clip_saga_cmd=f'{DtmDsmProcess.SAGA_PATH}\saga_cmd --flags=r shapes_grid "Clip Grid with Polygon" \
                -INPUT:"{grid_file}" -POLYGONS:"{shp_file}" -OUTPUT:"{output_file}"'
            clip_saga, error_clip_saga = DtmDsmProcess.execute_command(clip_saga_cmd)
            status=f"Clipped to buffer file: {grid_file_name}"
        except Exception as e:
            status=f"Can't clipped to buffer file: {grid_file_name} due to error:\n {e}"
        return status, output_file


    @staticmethod 
    def output_asc(grid_file):
        status=''
        output_file=re.sub('__(?!.*__).*', ".asc", grid_file).replace("_temp\\","")
        grid_file_name=os.path.basename(grid_file)

        try:
            output_asc_cmd=f'{DtmDsmProcess.SAGA_PATH}\saga_cmd --flags=r io_grid "Export ESRI Arc/Info Grid" -GRID:"{grid_file}" -FILE:"{output_file}" -FORMAT:1 -GEOREF:0 -PREC:4 -DECSEP:0'
            output_asc, error_output_asc = DtmDsmProcess.execute_command(output_asc_cmd)
            status=f"Convert to *.asc file: {grid_file_name}"
        except Exception as e:
            status=f"Can't convert to *.asc file: {grid_file_name} due to error:\n {e}"
        return status, output_file   

    @staticmethod 
    def output_xyz(grid_file):
        status=''
        output_file=re.sub('__(?!.*__).*', ".xyz", grid_file).replace("_temp\\","")
        grid_file_name=os.path.basename(grid_file)

        try:
            output_xyz_cmd=f'{DtmDsmProcess.SAGA_PATH}\saga_cmd --flags=r io_grid "Export Grid to XYZ" -GRIDS:"{grid_file}" -FILENAME="{output_file}" -HEADER:0'
            output_xyz, error_output_xyz = DtmDsmProcess.execute_command(output_xyz_cmd)
            status=f"Convert to *.xyz file: {grid_file_name}"
        except Exception as e:
            status=f"Can't convert to *.xyz file: {grid_file_name} due to error:\n {e}"
        return status, output_file   

    @staticmethod 
    def output_tif_hillshade(asc_file):
        status=''
        output_file=asc_file.replace(".asc","_hillshade.tif")
        asc_file_name=os.path.basename(asc_file)

        try:
            output_tif_hillshade_cmd=f'{DtmDsmProcess.GDAL}\gdaldem.exe hillshade "{asc_file}" "{output_file}" -co "TFW=TRUE"'
            output_tif_hillshade, error_output_tif_hillshade = DtmDsmProcess.execute_command(output_tif_hillshade_cmd)
            status=f"Convert to *.asc file: {asc_file_name}"
        except Exception as e:
            status=f"Can't convert to *.asc file: {asc_file_name} due to error:\n {e}"
        return status, output_file  
    
    @staticmethod 
    def output_tif(asc_file):
        status=''
        output_file=asc_file.replace(".asc",".tif")
        asc_file_name=os.path.basename(asc_file)

        try:
            output_tif_hillshade_cmd=f'{DtmDsmProcess.GDAL}\gdal_translate.exe -of GTiff -co "TFW=TRUE" "{asc_file}" "{output_file}" '
            output_tif_hillshade, error_output_tif_hillshade = DtmDsmProcess.execute_command(output_tif_hillshade_cmd)
            status=f"Convert to *.asc file: {asc_file_name}"
        except Exception as e:
            status=f"Can't convert to *.asc file: {asc_file_name} due to error:\n {e}"
        return status, output_file  



 
#DTM_DSM_Process.las_version("_test\\23762E1-01.las")
#print(DTM_DSM_Process.las_version("_test\\23762E1-01.las"))
#print(DTM_DSM_Process.read_las(r'd:\MSZ\_Repos\DTM_DSM_project\_test\_temp\23762E1-01_1_2.las'))
#print(DTM_DSM_Process.filter_first_return(r'd:\MSZ\_Repos\DTM_DSM_project\_test\_temp\23762E1-01_1_2.sg-pts'))
#print(DTM_DSM_Process.filter_ground_classes(r'd:\MSZ\_Repos\DTM_DSM_project\_test\_temp\23762E1-01_1_2.sg-pts',"2,12"))
#print(DTM_DSM_Process.grid_creation(r'd:\MSZ\_Repos\DTM_DSM_project\_test\_temp\23762E1-01_1_2__ground.sg-pts','DSM',1))
#print(DTM_DSM_Process.close_gaps(r'd:\MSZ\_Repos\DTM_DSM_project\_test\_temp\23762E1-01_1_2__grid_1.sgrd'))
#print(DTM_DSM_Process.gaussian_filter(r'd:\MSZ\_Repos\DTM_DSM_project\_test\_temp\23762E1-01_1_2__closed_gaps.sgrd'))
#print(DTM_DSM_Process.clip_saga(r'd:\MSZ\_Repos\DTM_DSM_project\_test\_temp\23762E1-01_1_2__gaussian.sgrd',
 #r'd:\MSZ\_Repos\DTM_DSM_project\_test\_temp\23762E1-01_1_2.shp'))

#print(DTM_DSM_Process.output_asc(r'd:\MSZ\_Repos\DTM_DSM_project\_test\_temp\23762E1-01_1_2__clipped.sgrd','DTM'))
#print(DTM_DSM_Process.output_xyz(r'd:\MSZ\_Repos\DTM_DSM_project\_test\_temp\23762E1-01_1_2__clipped.sgrd','DTM'))
#print(DTM_DSM_Process.output_tif_hillshade(r'd:\MSZ\_Repos\DTM_DSM_project\_test\23762E1-01_1_2_DTM.asc'))
#print(DTM_DSM_Process.output_tif(r'd:\MSZ\_Repos\DTM_DSM_project\_test\23762E1-01_1_2_DTM.asc'))
