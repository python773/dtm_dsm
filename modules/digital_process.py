from modules.DTM_DSM_ABC import DtmDsmProcess
from modules.functions import listing, createTemp, copy_shp_temp, prj_creator
import os
from modules.postgres import get_powerline_buffer


class DigitalProcess(DtmDsmProcess):
    
    def __init__(self, files_parameters):
        super().__init__()
        self.file = files_parameters[0]
        self.parameters = files_parameters[1]
        self.temp = createTemp(self.parameters['main_folder'])
        
        #check Version, read LAS
        self.st_lv, self.lv = self.las_version(self.file) 
        self.st_rl, self.rl = self.read_las(self.lv)
        
        #Assign SHP
        if self.parameters['clip_option']=='clip_to_shp':
            self.shp = listing(self.parameters['main_folder'], 'shp', False)[0]
            self.shp = copy_shp_temp(self.shp,os.path.basename(self.shp).split(".")[0])
    
    def run (self):   
        #Process run depends on settings
        if self.parameters['dsm_creation']:
            self.create_dsm_dtm('DSM') 
        if self.parameters['dtm_creation']:
            self.create_dsm_dtm('DTM')   

        
    def create_dsm_dtm(self,model): #Main process
    
        #DSM - Filter First Return, DTM - filter ground classes
        if model=='DSM':
            self.st_ffr, self.ffr = self.filter_first_return(self.rl, model) #Filter first return
        elif model=='DTM':
            self.st_ffr, self.ffr = self.filter_ground_classes(self.rl,self.parameters['ground_classes'], model)
        self.st_gc, self.gc = self.grid_creation(self.ffr, model, self.parameters['cell_size']) #grid creation
        
        #Close gaps
        if self.parameters['close_gaps']:
            self.st_cg, self.cg = self.close_gaps(
                self.gc,self.parameters['close_gaps_threshold']) 
        else:
            self.cg = self.gc
            
        #Gaussian blur
        if self.parameters['gaussian_filter']:
            self.st_gf, self.gf = self.gaussian_filter(
                self.cg,self.parameters['sigma'],
                self.parameters['kernel_type'],
                self.parameters['kernel_radius'])
        else:
            self.gf = self.cg
        
        #Clipper
        self.clipped = self.clipper_option(self.gf)

        #Outputter
        self.outputter(self.clipped)
    

    def clipper_option(self,grid_file):
        if self.parameters['clip_option'] == 'clip_to_shp':
            self.st_cs, self.cs = self.clip_saga(grid_file, self.shp)
            return self.cs
        elif self.parameters['clip_option'] == 'clip_to_database_buffer':
            self.powerline = os.path.basename(self.parameters['main_folder'])
            self.st_pb, self.pb = get_powerline_buffer(
                self.parameters['host'],
                self.parameters['port'],
                self.parameters['database'],
                self.powerline,
                float(self.parameters['buffer_r']),
                self.temp,
                self.parameters['epsg'])

            self.st_cs, self.cs = self.clip_saga(grid_file, self.pb)
            print(self.cs)
            return self.cs
        else:
            return grid_file


    def outputter(self, grid_file):
        if self.parameters['asc']:
            self.st_asc, self.asc = self.output_asc(grid_file)
        if self.parameters['xyz']:
            self.st_xyz, self.xyz = self.output_xyz(grid_file)
        if self.parameters['tif']:
            self.st_tif, self.tif = self.output_tif(self.asc)
        if self.parameters['tif_hillshade']:
            self.st_tif, self.tif = self.output_tif_hillshade(self.asc)

if __name__=='__main__':
    a=(r'd:\MSZ\_Repos\DTM_DSM_project\test_batch\14308__40KV_SÖRBY-BODA-HJORTKVARN\0001487809.las', 
    {'input_path': r'd:\MSZ\_Repos\DTM_DSM_project\test_batch\14308__40KV_SÖRBY-BODA-HJORTKVARN', 
    'dsm_creation': True, 'dtm_creation': True, 
    'ndsm_creation': False, 
    'cell_size': '0.5',  
    'ground_classes': '', 
    'epsg': '3006', 
    'close_gaps': True,
    'close_gaps_threshold': '0.100000',
    'gaussian_filter': True, 'sigma': '1', 
    'kernel_type': '1', 
    'kernel_radius': '5', 
    'clip_option': 'clip_to_database_buffer', 
    'buffer_r': '50', 
    'host': 'megadeth', 
    'port': '5433', 
    'database': '771-swe_eon', 
    'asc': True, 
    'xyz': False, 
    'tif': False, 
    'tif_hillshade': False, 
    'main_folder': r'd:\MSZ\_Repos\DTM_DSM_project\test_batch\14308__40KV_SÖRBY-BODA-HJORTKVARN'})
    #b=DigitalProcess(a)
    #b.run()



