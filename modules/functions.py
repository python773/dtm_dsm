from calendar import c
import os, glob, copy, shutil
from osgeo import osr
import re
os.environ['PROJ_LIB'] = r'd:\MSZ\_Repos\DTM_DSM_project\venv\Lib\site-packages\osgeo\data\proj'
os.environ['GDAL_DATA'] = r'd:\MSZ\_Repos\DTM_DSM_project\venv\Lib\site-packages\osgeo\data\gdal'


def epsg_converter(EPSG):
    CRS = osr.SpatialReference()
    CRS.ImportFromEPSG(int(EPSG))
    return str(CRS)

def prj_creator(input_file, EPSG):
    CRS = str(epsg_converter(EPSG))
    CRS = ''.join(CRS.split())
    prj_file = re.sub('\.\w{3}$','.prj',input_file)
    with open(prj_file, 'w') as w:
        w.write(CRS)


def permission_checker(path):
    try:
        os.mkdir(os.path.join(path, 'test'))
        os.rmdir(os.path.join(path, 'test'))
        return ""
    except PermissionError:
        return "Error - you don't have permission to write files in this folder"

def createTemp(path):
    #file or directory scenerio
    directory = os.path.dirname(path) if os.path.isfile(path) else path
    try:
        temp=os.path.join(directory,'_temp')
        if not os.path.exists(temp):
            os.makedirs(temp)
        return temp
    except Exception as e:
        return f"Can't create temp folder from {path} due to error {e}"

def listing(input_path, extension, subdirectories=True):
    if subdirectories:
        return glob.glob(os.path.join(input_path,'**',f'*.{extension}'), recursive=True)
    else:
        return glob.glob(os.path.join(input_path,f'*.{extension}'))

def files_parameters(file_list, parameters):
    fp = [] #file_parameters list
    for file in file_list:
        copy_parameters = copy.copy(parameters)
        copy_parameters['main_folder'] = os.path.dirname(file)
        fp.append((file,) + (copy_parameters,))
    return fp

def check_shp_in_folder(input_path): #check if shapefile is in folder
    status=[]
    extensions = ['shp', 'dbf', 'shx']
    directories = glob.glob(os.path.join(input_path, "*/"))
    las_files = glob.glob(os.path.join(input_path, "*.las"))
    
    #if las in input_path
    if las_files:
        for extension in extensions:
            if not listing(input_path,extension, False):
                status.append(f'Error - missing *.{extension} in folder: {input_path}')   

    #if only subdirectories
    if directories:  
        for directory in directories:
            for extension in extensions:
                if not listing(directory,extension, False):
                    status.append(f'Error - missing *.{extension} in folder: {directory}')
    return '\n'.join(status)


def copy_shp_temp(input_path_shp, suffix=''):
    suffix = ("__" + suffix) if len(suffix)>0 else suffix 
    output_file_name = os.path.basename(input_path_shp).split('.')[0]+suffix
    
    output_path_folder = createTemp(input_path_shp)
    
    input_path_shp = input_path_shp.replace('.shp','.*')
    for file in glob.glob(input_path_shp):
        out_file = os.path.join(output_path_folder,output_file_name+"."+file.split(".")[1])
        shutil.copy(file,out_file)
        if file.endswith('shp'):
            return out_file

#a=copy_shp_temp(r'd:\MSZ\_Repos\DTM_DSM_project\test_batch\14308__40KV_SÖRBY-BODA-HJORTKVARN_Tile_50m.shp','123')



